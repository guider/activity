'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dev = undefined;

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _vux = require('vux');

var _vue = require('vue');

var _vue2 = _interopRequireDefault(_vue);

var _qs = require('qs');

var _qs2 = _interopRequireDefault(_qs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dev = exports.dev = false;
_vue2.default.use(_vux.LoadingPlugin);
_vue2.default.use(_vux.ToastPlugin);
_vue2.default.use(_vux.AlertPlugin);

// axios 配置
_axios2.default.defaults.timeout = 8000;
_axios2.default.defaults.withCredentials = true;
var baseURL = 'http://api.prguanjia.com';
// http response 拦截器
_axios2.default.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (error.response) {
    switch (error.response.status) {
      case 404:
        console.log('请求404');
        break;
      case 500:
        console.log('请求500');
        break;
    }
  }
  console.log(error);
  return _promise2.default.reject({ code: '-100', message: '网络异常请稍后再试！' });
});
// 请求时的拦截
_axios2.default.interceptors.request.use(function (config) {
  // 发送请求之前做一些处理
  return config;
}, function (error) {
  // 当请求异常时做一些处理
  return _promise2.default.reject(error);
});

// 封装请求
function fetch(url, options, hideProgress) {

  // 显示
  if (!hideProgress) {
    _vue2.default.$vux.loading.show({
      text: 'Loading'
    });
  }

  var opt = options || {};
  return new _promise2.default(function (resolve, reject) {
    if (!dev) {
      url = url.replace('http://tservice.prguanjia.com', 'http://service.wx.prguanjia.com');
    }
    (0, _axios2.default)({
      method: opt.type || 'get',
      url: url.includes('http') ? url : 'http://api.prguanjia.com/' + url,
      // url: 'http://api.prguanjia.com/'+url,
      params: opt.type !== 'post' ? opt.params : {},
      // 判断是否有自定义头部，以对参数进行序列化。不定义头部，默认对参数序列化为查询字符串。
      // data: (opt.headers ? opt.data : stringify(opt.data)) || {},
      data: opt.params || {},
      responseType: opt.dataType || 'json',
      // 设置默认请求头
      headers: opt.headers || { 'Content-Type': 'application/x-www-form-urlencoded' },
      // proxy: {
      //   host: 'api.prguanjia.com',
      //   port: 80
      // },
      transformRequest: [function (data) {
        data = _qs2.default.stringify(data);
        return data;
      }]
    }).then(function (response) {
      resolve(response.data);

      _vue2.default.$vux.loading.showing = false;

      // if (response.data.code === 0) {
      // } else if (response.data.code === '000') {
      //   resolve(response.data)
      // } else {
      //   reject(response.data)
      // }
      // 隐藏
      _vue2.default.$vux.loading.hide();
    }).catch(function (error) {
      _vue2.default.$vux.loading.showing = false;
      console.log(error);
      reject(error);
      // 隐藏
      _vue2.default.$vux.loading.hide();
    });
  });
}

exports.default = fetch;
//# sourceMappingURL=http.js.map
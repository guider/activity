"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  list: [{
    title: "Q1：提现到哪里？",
    desc: "余额可直接提现至微信余额，可在微信交易中查询。"
  }, {
    title: "Q2：提现有限制吗？",
    desc: '1.提现金额以元为单位，须为10的整数倍。<br/>' + '如，当前余额为18.88元，则最大可提现金额为10元，当前余额变为8.88元。<br/>' + '2.提现金额上限单笔2千。<br/>'
  }, {
    title: "Q3：提现遇到问题怎么办？？",
    desc: "如遇到提现问题，请添加客服微信licaishi1124"
  }],
  table: [],
  desc: ""
};
//# sourceMappingURL=WithdrawDepositConfig.js.map
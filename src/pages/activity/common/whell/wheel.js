'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Class Wheel
 * @class
 * @classdesc 大转盘游戏逻辑部分
 * @author pfan
 * @todo 注意：移动端真机，不支持requestAnimationFrame.
 *
 * @example
 *  new Wheel(this,{
 *    areaNumber: 8,   //抽奖间隔
 *    speed: 16,       //转动速度
 *    awardNumer: 2,   //中奖区域从1开始
 *    mode: 1,         //1是指针旋转，2为转盘旋转
 *    callback: (idx, award) => {
 *      //结束回调， 参数对应宫格索引，对应奖项
 *    }
 *  })
 */
var Wheel = function () {
  /**
   * @constructs Wheel构造函数
   * @param  {Object} pageContext page路由指针
   * @param  {Object} opts      组件所需参数
   * @param  {Number} opts.areaNumber  抽奖间隔
   * @param  {Number} opts.speed       转动速度
   * @param  {Number} opts.awardNumer  中奖区域从1开始
   * @param  {Number} opts.mode     1是指针旋转，2为转盘旋转
   * @param  {Function} opts.callback    结束回调
   */
  function Wheel(pageContext, opts) {
    (0, _classCallCheck3.default)(this, Wheel);

    this.page = pageContext;
    this.deg = 0;
    this.areaNumber = opts.areaNumber; // 奖区数量
    this.speed = opts.speed || 16; // 每帧速度
    this.awardNumer = opts.awardNumer; //中奖区域 从1开始
    this.mode = opts.mode || 2;
    this.singleAngle = ''; //每片扇形的角度
    this.isStart = false;
    this.endCallBack = opts.callback;

    this.init();

    this.page.start = this.start.bind(this);
  }

  (0, _createClass3.default)(Wheel, [{
    key: 'init',
    value: function init() {
      var areaNumber = this.areaNumber,
          singleAngle = this.singleAngle,
          mode = this.mode;

      singleAngle = 360 / areaNumber;
      this.singleAngle = singleAngle;
      this.data = {
        wheel: {
          singleAngle: singleAngle,
          mode: mode
        }
      };
    }
  }, {
    key: 'start',
    value: function start() {
      var _this = this;

      var deg = this.deg,
          awardNumer = this.awardNumer,
          singleAngle = this.singleAngle,
          speed = this.speed,
          isStart = this.isStart,
          mode = this.mode;

      if (isStart) return;
      this.isStart = true;
      var endAddAngle = (awardNumer - 1) * singleAngle + singleAngle / 2 + 360; //中奖角度
      var rangeAngle = (Math.floor(Math.random() * 4) + 4) * 360; // 随机旋转几圈再停止
      var cAngle = void 0;
      deg = 0;
      this.timer = setInterval(function () {
        if (deg < rangeAngle) {
          deg += speed;
        } else {
          cAngle = (endAddAngle + rangeAngle - deg) / speed;
          cAngle = cAngle > speed ? speed : cAngle < 1 ? 1 : cAngle;
          deg += cAngle;

          if (deg >= endAddAngle + rangeAngle) {
            deg = endAddAngle + rangeAngle;
            _this.isStart = false;
            clearInterval(_this.timer);
            _this.endCallBack();
          }
        }

        _this.page.wheel = {
          singleAngle: singleAngle,
          deg: deg,
          mode: mode
        };
        // console.log(this.page.wheel);
        // console.log(this.page.data.wheel);

        // console.log('deg : '+deg);
        // console.log(this.page.data.wheel.deg);
        // this.page.refresh({
        //   singleAngle: singleAngle,
        //   deg: deg,
        //   mode: mode
        // });
      }, 1000 / 60);
    }
  }, {
    key: 'reset',
    value: function reset() {
      var mode = this.mode;

      this.deg = 0;
      this.data = {
        wheel: {
          singleAngle: this.singleAngle,
          deg: 0,
          mode: mode
        }
      };
    }
  }, {
    key: 'switch',
    value: function _switch(mode) {
      this.mode = mode;
    }
  }]);
  return Wheel;
}();

exports.default = Wheel;
//# sourceMappingURL=wheel.js.map
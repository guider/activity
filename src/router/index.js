'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _vue = require('vue');

var _vue2 = _interopRequireDefault(_vue);

var _vueRouter = require('vue-router');

var _vueRouter2 = _interopRequireDefault(_vueRouter);

var _vueGa = require('vue-ga');

var _vueGa2 = _interopRequireDefault(_vueGa);

var _ShuangDan = require('../pages/activity/ShuangDan');

var _ShuangDan2 = _interopRequireDefault(_ShuangDan);

var _ShuangDanWheel = require('../pages/activity/ShuangDanWheel');

var _ShuangDanWheel2 = _interopRequireDefault(_ShuangDanWheel);

var _ShuangDanResult = require('../pages/activity/ShuangDanResult');

var _ShuangDanResult2 = _interopRequireDefault(_ShuangDanResult);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_vue2.default.use(_vueRouter2.default);
var router = new _vueRouter2.default({
  mode: 'history',
  base: '/activity',
  saveScrollPosition: true,
  routes: [{
    path: '/',
    redirect: '/shuangdan'
  }, {
    path: '/shuangdan',
    name: 'shuangdan',
    component: _ShuangDan2.default,
    meta: {
      title: '双旦狂欢',
      navHidden: true
    }
  }, {
    path: '/shuangdanwheel',
    name: 'shuangdanwheel',
    component: _ShuangDanWheel2.default,
    meta: {
      title: '双旦狂欢',
      navHidden: true
    }
  }, {
    path: '/result',
    name: 'result',
    component: _ShuangDanResult2.default,
    meta: {
      title: '领奖',
      navHidden: true
    }
  }, {
    path: '/*',
    redirect: '/shuangdan'
  }],

  scrollBehavior: function scrollBehavior(to, from, savePostion) {
    return savePostion | { x: 0, y: 0

      // if (savedPosition) {
      //   return savedPosition
      // } else {
      //   if (from.meta.keepAlive) {
      //     from.meta.savedPosition = document.body.scrollTop;
      //   }
      //   return { x: 0, y: to.meta.savedPosition ||0}ttemplate

      // }
    };
  }
});

(0, _vueGa2.default)(router, 'UA-110807983-1');

exports.default = router;
//# sourceMappingURL=index.js.map
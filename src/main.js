'use strict';

var _vue = require('vue');

var _vue2 = _interopRequireDefault(_vue);

var _App = require('./App.vue');

var _App2 = _interopRequireDefault(_App);

var _router = require('./router');

var _router2 = _interopRequireDefault(_router);

var _vux = require('vux');

var _http = require('./api/http');

var config = _interopRequireWildcard(_http);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _vueScroller = require('vue-scroller');

var _vueScroller2 = _interopRequireDefault(_vueScroller);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// axios.defaults.baseURL = 'http://localhost:7890/';
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
_axios2.default.defaults.baseURL = '/api';

/* ----------- Config ---------- */


_vue2.default.use(_vueScroller2.default);


_vue2.default.use(_vux.WechatPlugin);

/* ----------- Config ---------- */
_vue2.default.config.productionTip = !config.dev;

// 省略...
_router2.default.afterEach(function (route) {
  // 从路由的元信息中获取 title 属性
  // if (route.name) {
  //   document.title = route.name;
  if (route.meta && route.meta.title || route.name) {
    document.title = route.meta && route.meta.title ? route.meta.title : route.name;
    // 如果是 iOS 设备，则使用如下 hack 的写法实现页面标题的更新
    if (navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
      var hackIframe = document.createElement('iframe');
      hackIframe.style.display = 'none';
      hackIframe.src = '/robots.txt?r=' + Math.random();

      document.body.appendChild(hackIframe);

      setTimeout(function (_) {
        document.body.removeChild(hackIframe);
      }, 300);
    }
  }
});

_router2.default.beforeEach(function (to, from, next) {
  if (!_vux.cookie.get('unionid') && !config.dev) {
    // http://service.wx.prguanjia.com/redpack/auth?callback=
    location.href = 'http://service.wx.prguanjia.com/account/auth?callback=' + location.href;
    return;
  }
  next();
});

// let wx =Vue.$wechat;

//设置微信分享全局函数
var http = _axios2.default;
_vue2.default.prototype.wxShare = function (wx, url, callback) {
  var originUrl = url;
  url = 'http://service.wx.prguanjia.com/share/setShareData?url=' + encodeURIComponent(url);
  // this.$wechat,title, desc, link, shareimg,
  http.get(url).then(function (res) {
    // 获得签名配置
    var Data = res.data.signPackage;
    var shareData = res.data.shareData;

    // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，
    wx.config({
      debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
      appId: Data.appId, // 必填，公众号的唯一标识
      timestamp: Data.timestamp, // 必填，生成签名的时间戳
      nonceStr: Data.nonceStr, // 必填，生成签名的随机串
      signature: Data.signature, // 必填，签名，见附录1
      jsApiList: ['onMenuShareAppMessage', 'onMenuShareTimeline'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });
    ready(wx, shareData, originUrl, callback);
  }).catch(function (err) {
    console.log(err);
  });
};

function ready(wx, shareData, originUrl, calllback) {
  wx.ready(function () {
    // 所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，
    // 则可以直接调用，不需要放在ready函数中。
    wx.onMenuShareAppMessage({ // 分享给朋友
      title: shareData.title, // 分享标题
      desc: shareData.description, // 分享描述
      link: originUrl, // 分享链接 默认以当前链接
      imgUrl: shareData.image, // 分享图标
      // 用户确认分享后执行的回调函数
      success: function success() {
        if (calllback) {
          calllback();
        }
        console.log('分享成功');
      },
      // 用户取消分享后执行的回调函数
      cancel: function cancel() {
        console.log('分享到朋友取消');
      }
    });
    //分享到朋友圈
    wx.onMenuShareTimeline({
      title: shareData.title, // 分享标题
      desc: shareData.description,
      link: originUrl,
      imgUrl: shareData.image, // 分享图标
      // 用户确认分享后执行的回调函数
      success: function success() {
        console.log("分享成功");
        if (calllback) {
          calllback();
        }
      },
      // 用户取消分享后执行的回调函数
      cancel: function cancel() {
        console.log('分享到朋友圈取消');
      }
    });
    wx.error(function (res) {
      // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，
      // 也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
      console.log(res); //
    });
  });
}

/* eslint-disable no-new */
new _vue2.default({
  el: '#app',
  router: _router2.default,
  template: '<App/>',
  components: { App: _App2.default }
});
//# sourceMappingURL=main.js.map